# Rules

## Rules for Selecting Games

These rules are to balance the games between the competitors. They must not be broken!

* Each competitor must be practiced in at least 3 of the games.
* Each competitor cannot be a non-practiced in more games then practiced.
* Every competitior must have a game where they are favoured highest to win.
* Every competitor must have a game where they are least likely to win.
* There are 5 games a tournament, but the pool of the games is infinite.

## Point System

We do more points for win because it allows a larger group of people to join without changing the point system.

    | Place | Points |
    |-------|--------|
    | 1st   | 3      |
    | 2nd   | 2      |
    | 3rd   | 1      |
    | 4+    | 0      |
    Last Place = Gets to pick next game

## Time Limits

    The tournament will have a limit of 3 hours, unless an extension or reduction is agreed upon by all participants before hand.

    An extension can happen during, but all participants must agree.