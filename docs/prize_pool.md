# Prize Pool

## Winner Prize

Current prize pool is a $20 item (maximum) that is game related.

Your name on the Champions list.

## Loser Consolation Boost

Loser gets to choose the category of the games in the next tournament.

Example: Loser chooses Mario Games as the category for the next tournament.