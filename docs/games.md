# Games

[Starcraft](games/starcraft.md)

[League of Legends](games/league.md)

[Fortnite](games/fortnite.md)

[COD: Black Ops](games/cod_black_ops.md)

[Mariokart](games/mario_kart_64.md)

[Civilization](civilization.md)

[Everyone is a Noob Game](everyone_is_a_noob.md)

## Reason for Format of Games

Each game will have a list of Experts, Noobs, Winners, and Losers.

These are listed to help determine if the game is fair to add to the tournament.

When choosing 5 games for the tournament, choose 5 that end up with an even breakdown of winners and losers.

### Example

Games chosen = Starcraft, League, COD, Mariokart, Fortnite

Using the Winners and Losers section of each game you come out with these totals of each player winning and losing. This would be an acceptable balance to the 5 games as no person has greater then 3 wins or losses.

```odds
Kevin
wins = 2
losses = 2

Colin
wins = 2
losses = 3

Andrew
wins = 3
losses = 2
```