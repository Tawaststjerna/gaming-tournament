# Korean Style

    * Round robin to find losers bracket
    * Best loser to fight round robin winner
    * Made this from Starcraft tournaments

## When it Works

### Example

```example
5 games each time. Depending on game, 30 mins to 1.5 hour.

Round Robin
K v A
K wins

K v C
K wins

A v C
A wins

Losers bracket
A v C
C wins

Finals
K v C
K wins
```

## When it Does not Work

Example

```example
Round Robin
K v A
K wins

K v C
C wins

A v C
A wins
```

Now everyone is even and no loser bracket/best person can be chosen.

Do we do it again or have a catagory of the best person?

Maybe a lightning round? Probably could come up with one for each game
