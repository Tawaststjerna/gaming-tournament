# Battle Royal "Pop Up Cup"

Essentially Fortnite Pop Up Cup rules.

## Rules

    1 Hour time limit
    Usually 5 rounds but depending on the speed of the game it could be more or less
    4 elms = 1
    6 elms = 1
    8 elms = 1
    Top 5 = 2
    Top 3 = 2
    Victory Royal = 2
