# Civilization

## Breakdown of Skill Levels

### Experts

* Kevin
* Colin

### Noob

* Andrew

## Odds

### Winner

* Colin

### Loser

* Andrew

## Tournament Type

Single game. Short to Medium time. FFA.

Should decide on computer players or not.