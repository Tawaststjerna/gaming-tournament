# Mario Party

## Breakdown of Odds

### Winner

* Anyone

### Loser

* Anyone

## Tournament Type

### Standard Game Mode

Simple boards.

When the tournament is 1 day, use 15 turn board.
When the tournament is multiple days, where 1 game is 1 day, use 20 turn board.
