# Gaming Tournament 2019

## 🏆 Champions 🏆

Here are the greatest gamers (in our own opinion of course).

| Name                	| Tournament            	| Date       	|
|---------------------	|-----------------------	|------------	|
| Kevin Tawaststjerna 	| The Origin Tournament 	| 2019-04-19 	|

### Summary

The gaming tournament will consist of 5 games, each game has its own format. This tournament is for glory, honor, and some bragging rights... oh and sometimes a prize pool!

## Glossary

1) [Games](docs/games.md)
1) [Rules](docs/rules.md)
1) [Types of Tournaments](docs/types_of_tournaments.md)
1) [Prize Pool](docs/prize_pool.md)