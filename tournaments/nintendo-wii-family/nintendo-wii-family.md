# The Origin Tournament

A tournament focused only on Nintendo Wii and Wii U games.

## Date

Starts on Wednesday, June 19th, 2019.

The games will be played throughout the weeks instead of all on one day. The dates of the matches are as determined by participants before we play them to manage flexibility of schedules.

## Location

62 Madison Ave OR 72 Brandon Ave

## Participants

Kevin

Colin

Andrew

Charlotte

## Games

Mario Party

Wii Sports

Wii Play

Super Mario Bros U

???

## Results

| Game               	| Andrew 	| Kevin 	| Colin 	| Charlotte 	| Totals
|--------------------	|--------	|-------	|-------	|-----------	|--------
| Mario Party        	| 1     	| 3    	  | 0     	| 2         	| 1-3-0-2
| ??????????????????? |        	|       	|       	|           	|
| ???????????????????	|        	|       	|       	|           	|
| ??????????????????? |        	|       	|       	|           	|
| ??????????????????? |        	|       	|       	|           	|
| Overall            	| 1      	| 3     	| 0     	| 2         	|


## Breakdown of Balancing in Games

### Mario Party

**Winner:** Anyone

**Loser:** Anyone


### Wii Sports

**Winner:** Colin

**Loser:** Charlotte


### Wii Play

**Winner:** Anyone

**Loser:** Anyone


### Super Mario Bros U

**Winner:** Anyone

**Loser:** Anyone


### ???

**Winner:**

**Loser:**


## Breakdown of catagories by person

### Kevin

  Winner: I I I

  Loser: I I I

### Colin

  Winner: I I I I

  Loser: I I I

### Andrew

  Winner: I I I

  Loser: I I I I

### Charlotte

  Winner: I I I

  Loser: I I I I
