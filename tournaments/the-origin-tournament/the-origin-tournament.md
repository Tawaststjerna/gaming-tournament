# The Origin Tournament

Our first ever tournament in this league. The origin tournament will be used to guage how the tournament feels, if the number of players is good (3 players), if the prize pool fits, and if the game selection is fair. This will be a selection of games that do not have a specific genre or type.

## Date

Saturday, March 31st, 2019

## Location

99 Chandos, Apt 207

## Participants

Kevin

Colin

Andrew


## Games

Starcraft: UMS
1) Dodge the Units
1) Snipers
1) Zone Control

Mario Kart 64

Mario Party

Fortnite

COD: Black Ops 2

## Results

| Game           | Andrew🥈 | Kevin🥇| Colin🥉| Totals |
|----------------|----------|---------|---------|--------|
| Fortnite       | 1        | 2       | 3       | 1-2-3  |
|  Mario Kart    | 2        | 3       | 1       | 3-5-4  |
| Starcraft      | 3        | 2       | 1       | 6-7-5  |
| Mario Party    | 2        | 3       | 1       | 8-10-6 |
| COD: Black Ops | 1        | 3       | 2       | 9-13-8 |
| Overall        | 9        | 13      | 8       |        |

## Breakdown of Balancing in Games

### Starcraft: UMS

**Expert:** Kevin and Andrew

**Noob:** Colin

**Winner:** Kevin or Andrew

**Loser:** Colin

### Mario Kart 64

**Expert:** All

**Noob:** None

**Winner:** Andrew

**Loser:** Colin

### Mario Party

**Expert:** Colin

**Noob:** Kevin & Andrew

**Winner:** Colin

**Loser:** Kevin or Andrew

### Fortnite

**Expert:** All

**Noob:** None

**Winner:** Colin

**Loser:** Andrew

### COD

**Expert:** All

**Noob:** None

**Winner:** Andrew or Kevin

**Loser:** Colin

## Breakdown of catagories by person

### Kevin

  Expert: 4

  Noob: 1

  Winner: 2

  Loser: 1

### Colin

  Expert: 4

  Noob: 1

  Winner: 2

  Loser: 3

### Andrew

  Expert: 4

  Noob: 1

  Winner: 3

  Loser: 2
