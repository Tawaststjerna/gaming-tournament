# Online Super Tournament

Our first ever online tournament in this league! There will be only games that can be played online or alone. No in person gaming!

# Date

TBD

# Location

Your house & computer

# Participants

    Kevin
    Colin
    Andrew
    Jamie

# Games

## Starcraft 1 & 2

* Best of 3.
1) Starcraft 1
2) Starcraft 2
3) Coin Flip

Kevin and Andrew play this game a lot... But Andrew has more training currently.

The SC2 insert could give Colin a chance to win, especially if it goes to 2 games of SC 2.

    Practiced: Kevin + Andrew + Jamie
    Non-Practiced: Colin

---

## Fortnite

We all play this game together, but Colin is most trained and skilled.

I play some shooters so I can take a 2nd place in this fairly easily.

This leaves Andrew with last place most of the time.

    Practiced: Andrew + Colin + Kevin
    Non-Practiced: Jamie

---

## Bloons Tower Defense

We've all played this before. Colin the least but he has knowledge on tower defense.

    Practiced: Kevin + Andrew + Colin
    Non-Practiced: Jamie

---

## Bubble Booble

  Kevin and Andrew played this game as children and I don't believe Colin did.

  Andrew always did better at this game then Kevin, so in favour for Andrew.

    Practiced: Andrew + Kevin
    Non-Practiced: Colin + Jamie

---

## Card Games online

Completely depends on which game we play.

    Practiced: All
    Non-Practiced: All

---

## Surviv.io

Browser based top down battle royal. Should be fun. No one is practiced in this.

    Practiced: None
    Non-Practiced: All

---

## Quake 3

We're all totally Non-Practiceds at this game, even if we've played shooters before on PC.

    Practiced: None
    Non-Practiced: All

---

## Halo 3

Can be bought on Steam $10.99.
Kevin and Colin are beyond Practiceds, it will an almost guaranteed win for either of them. Andrew isn't a Non-Practiced but he isn't well practiced.

    Practiced: Colin + Kevin
    Non-practiced: Andrew + Jamie

---

## Slither .io

Andrew and Kevin played this game a bunch for a few weeks in the past. Not sure if Colin did.

    Practiced: Andrew + Kevin + Colin
    Non-Practiced: Jamie

---

## No Internet Chrome Game

The dinosaur jumping over stuff game. I'm going to say every is practiced at jumping over things in a game...

    Practiced: All
    Non-Practiced: None

---

## WCW vs NWO Revenge

    Practiced: Jamie
    Non-Practiced: Colin + Andrew + Kevin

---

## Bubble Trouble

Really this is only if Charlotte joins

    Practiced: Kevin
    Non-Practiced: Andrew + Colin + Jamie

Breakdown of Practiced VS Non-practiced as of 2020-07-31

| Player | Practiced | Non-practiced |
|--------|-----------|---------------|
| Kevin  | 8         | 4             |
| Andrew | 7         | 5             |
| Colin  | 5         | 7             |
| Jamie  | 4         | 8             |

# Results

🥇
🥈
🥉

| Game           |  Andrew  |  Kevin  |  Colin  | Jamie |  Total  | Winner  |
|----------------|----------|---------|---------|-------|---------|---------|
| Slither.io     | 2        | 3       | 1       | 0     | 2-3-1-0 | Kevin   |
| VIDEOGAME      | #        | #       | #       | #     | #       | #       |
| VIDEOGAME      | #        | #       | #       | #     | #       | #       |
| VIDEOGAME      | #        | #       | #       | #     | #       | #       |
| VIDEOGAME      | #        | #       | #       | #     | #       | #       |
|----------------|----------|---------|---------|-------|---------|---------|
| Overall        | #        | #       | #       | #     |---------|---------|